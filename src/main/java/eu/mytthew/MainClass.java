package eu.mytthew;

public class MainClass {
	public static void main(String[] args) {
		House house = House.builder()
				.floors(2)
				.rooms(8)
				.garage(true)
				.garden(true)
				.cellar(true)
				.surface(255.11)
				.build();
		System.out.println(house);
	}
}
