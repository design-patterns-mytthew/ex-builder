package eu.mytthew;

public class House {
	private int floors;
	private boolean garage;
	private int rooms;
	private boolean garden;
	private boolean cellar;
	private double surface;

	public static final class Builder {
		private int floors;
		private boolean garage;
		private int rooms;
		private boolean garden;
		private boolean cellar;
		private double surface;


		public Builder floors(int floors) {
			this.floors = floors;
			return this;
		}

		public Builder garage(boolean garage) {
			this.garage = true;
			return this;
		}

		public Builder rooms(int rooms) {
			this.rooms = rooms;
			return this;
		}

		public Builder garden(boolean garden) {
			this.garden = true;
			return this;
		}

		public Builder cellar(boolean cellar) {
			this.cellar = true;
			return this;
		}

		public Builder surface(double surface) {
			this.surface = surface;
			return this;
		}

		public House build() {
			House house = new House();
			house.floors = floors;
			house.rooms = rooms;
			house.garage = garage;
			house.garden = garden;
			house.cellar = cellar;
			house.surface = surface;
			return house;
		}
	}

	public static Builder builder() {
		return new Builder();
	}
}
